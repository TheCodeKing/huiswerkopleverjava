#!/bin/bash

cd /Applications/XAMPP/xamppfiles/htdocs/classes/

java -cp ./bin:./libs/log4j-1.2.17.jar:./libs/mysql-connector-java-5.1.32.jar edu.avans.aei.ivh5.model.main.LibraryServer -properties resources/breda.properties &

java -cp ./bin:./libs/log4j-1.2.17.jar:./libs/mysql-connector-java-5.1.32.jar edu.avans.aei.ivh5.model.main.LibraryServer -properties resources/oosterhout.properties &

